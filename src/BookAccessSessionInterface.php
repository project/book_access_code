<?php

namespace Drupal\book_access_code;

/**
 * Stores access codes in the current user's session.
 *
 * Allows the system to keep track of which access codes have been used per
 * user. The session is the only available storage for both anonymous and
 * authenticated users in this case, since all anonymous users share the same
 * user id (0).
 */
interface BookAccessSessionInterface {

  public const BOOK_ACCESS_CODES_KEY = 'book_access_codes';

  /**
   * Get access codes on session.
   *
   * @return array
   *   Array of access codes the current user has already applied.
   */
  public function getAccessCodes(): array;

  /**
   * Adds and access code to current user's session.
   *
   * @param string $accessCode
   *   Access code to add.
   */
  public function addAccessCode(string $accessCode): void;

  /**
   * Checks to see if access code to current user's account.
   *
   * @param string $accessCode
   *   Access code to check.
   *
   * @return bool
   *   TRUE if the given access code exists in the session, FALSE otherwise.
   */
  public function hasAccessCode(string $accessCode): bool;

}
