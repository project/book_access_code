<?php

namespace Drupal\book_access_code;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Access code entity.
 *
 * @see \Drupal\book_access_code\Entity\AccessCode.
 */
class AccessCodeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\book_access_code\Entity\AccessCodeInterface $entity */

    switch ($operation) {

      case 'edit':
      case 'update':
      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'edit access codes');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete access codes');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add access codes');
  }

}
