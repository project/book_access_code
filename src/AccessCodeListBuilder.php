<?php

namespace Drupal\book_access_code;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Access code entities.
 *
 * @ingroup book_access_code
 */
class AccessCodeListBuilder extends EntityListBuilder {

  /**
   * The book access code manager.
   *
   * @var \Drupal\book_access_code\BookAccessCodeManager
   */
  private $bookAccessCodeManager;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('book_access_code.manager')
    );
  }

  /**
   * Constructs a new ViewListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\book_access_code\BookAccessCodeManagerInterface $bookAccessCodeManager
   *   The book access code manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, BookAccessCodeManagerInterface $bookAccessCodeManager) {
    parent::__construct($entity_type, $storage);
    $this->bookAccessCodeManager = $bookAccessCodeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['code'] = $this->t('Code');
    $header['active'] = $this->t('Active');
    $header['books'] = $this->t('Books');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\book_access_code\Entity\AccessCodeInterface $entity */

    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.access_code.edit_form',
      ['access_code' => $entity->id()]
    );

    $row['code'] = $entity->getCode();
    $row['active'] = $entity->isPublished() ? '✔' : '✘';
    $row['books']['data'] = [
      '#theme' => 'item_list',
      '#items' => $entity->getBookLinks(),
      '#cache' => [
        'tags' => ['node_list', 'access_code_list'],
      ],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['description'] = [
      '#markup' => '<p>' . $this->t('Published books that have an access code can only be read by providing one of the associated active access codes.') . '</p>',
      '#weight' => -2,
    ];

    $build['footer'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Books Without Active Access Codes'),
      '#weight' => 99,
    ];

    $build['footer']['description'] = [
      '#markup' => '<p>' . $this->t('The following books do not have any ACTIVE access codes and therefore are accessible if published.') . '</p>',
      '#weight' => 2,
    ];

    $build['footer']['no_active_access_code_books'] = $this->booksWithoutActiveCodesRender();
    $build['footer']['no_active_access_code_books']['#weight'] = 3;

    return $build;
  }

  /**
   * Prints a listing of all books.
   *
   * @return array
   *   A render array representing the listing of all books content.
   */
  private function booksWithoutActiveCodesRender(): array {
    $book_list = [];
    foreach ($this->bookAccessCodeManager->booksWithoutActiveAccessCodes() as $book) {
      $book_list[] = Link::fromTextAndUrl($book['title'], $book['url']);
    }
    return [
      '#theme' => 'item_list',
      '#items' => $book_list,
      '#empty' => [
        '#markup' => '<em>' . $this->t('None. Either there are no books or all books have at least one active access code.') . '</em>',
      ],
      '#cache' => [
        'tags' => ['node_list', 'access_code_list'],
      ],
    ];
  }

}
