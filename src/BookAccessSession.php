<?php

namespace Drupal\book_access_code;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Default implementation of the book access session.
 */
class BookAccessSession implements BookAccessSessionInterface {

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  private $session;

  /**
   * Constructs a BookAccessSession.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   */
  public function __construct(SessionInterface $session) {
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessCodes(): array {
    return $this->session->get(self::BOOK_ACCESS_CODES_KEY, []);
  }

  /**
   * {@inheritdoc}
   */
  public function addAccessCode(string $accessCode): void {
    $accessCodes = $this->session->get(self::BOOK_ACCESS_CODES_KEY, []);
    $accessCodes[] = $accessCode;
    $this->session->set(self::BOOK_ACCESS_CODES_KEY, array_unique($accessCodes));
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccessCode(string $accessCode): bool {
    $accessCodes = $this->session->get(self::BOOK_ACCESS_CODES_KEY, []);
    return in_array($accessCode, $accessCodes, TRUE);
  }

}
