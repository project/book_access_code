<?php

namespace Drupal\book_access_code\Form;

use Drupal\book_access_code\BookAccessCodeManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BookAccessCodeForm.
 */
class BookAccessCodeForm extends FormBase {

  /**
   * Drupal\book_access_code\BookAccessCodeManagerInterface definition.
   *
   * @var \Drupal\book_access_code\BookAccessCodeManagerInterface
   */
  protected $bookAccessCodeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The title of the book.
   *
   * @var string
   */
  private $bookTitle;

  /**
   * The id of the book.
   *
   * @var int
   */
  private $bookId;

  /**
   * BookAccessCodeForm constructor.
   *
   * @param \Drupal\book_access_code\BookAccessCodeManagerInterface $bookAccessCodeManager
   *   The book access code manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(BookAccessCodeManagerInterface $bookAccessCodeManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->bookAccessCodeManager = $bookAccessCodeManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('book_access_code.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'book_access_code_form';
  }

  /**
   * Route title callback.
   *
   * @return string
   *   The book access code title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see book_access_code.routing.yml
   */
  public function pageTitle() {
    return $this->t('Access %bookTitle', ['%bookTitle' => $this->bookTitle()]);
  }

  /**
   * Gets the title of the book trying to be accessed.
   *
   * @return string
   *   The book title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function bookTitle(): string {
    if (!isset($this->bookTitle)) {
      $this->bookTitle = (string) $this->entityTypeManager->getStorage('node')->load($this->bookId())->label();
    }

    return $this->bookTitle;
  }

  /**
   * Gets the id of the book trying to be accessed.
   *
   * @return int
   *   The book id.
   */
  private function bookId() {
    if (!isset($this->bookId)) {
      $this->bookId = (int) $this->getRequest()->query->get('bid');
    }
    return $this->bookId;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('book_access_code.settings');

    $form['description'] = [
      '#markup' => '<p class="book_access_description">' . $config->get('access_page_description') . '</p>',
    ];

    $form['access_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Access Code'),
      '#description' => $this->t('Provide the access code you were provided to access this book.'),
      '#maxlength' => 12,
      '#size' => 12,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['bid'] = [
      '#type' => 'hidden',
      '#value' => $this->bookId(),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $form['#cache']['contexts'][] = 'url.query_args:destination';
    $form['#cache']['contexts'][] = 'url.query_args:bid';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValueEmpty('access_code')) {
      $form_state->setErrorByName('access_code', $this->t('You must provide an access code'));
    }

    if ($form_state->isValueEmpty('bid')) {
      $form_state->setErrorByName('access_code', $this->t('No book provided.'));
    }

    if (!$this->bookAccessCodeManager->accessCodeAppliesToBook($form_state->getValue('bid'), $form_state->getValue('access_code'))) {
      $form_state->setErrorByName('access_code', $this->t('Invalid Access Code.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->bookAccessCodeManager->storeAccessCodeToCurrentUserSession($form_state->getValue('access_code'));

    // If destination is not set, have the form redirect to the main book page.
    if (!$this->getRequest()->query->has('destination')) {
      $form_state->setRedirect('entity.node.canonical', ['node' => $this->bookId()]);
    }
  }

  /**
   * Determines if we can access this form by checking for valid query params.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access() {
    return AccessResult::allowedIf(($this->getRequest()->query->has('bid') && $this->bookAccessCodeManager->isBookId($this->getRequest()->query->get('bid'))))
      ->addCacheContexts(['url.query_args:destination', 'url.query_args:bid']);
  }

}
