<?php

namespace Drupal\book_access_code\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Delete form for Access codes.
 */
class AccessCodeDeleteForm extends ContentEntityDeleteForm {

}
