<?php

namespace Drupal\book_access_code\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BookAccessCodeSettingsForm.
 */
class BookAccessCodeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'book_access_code.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'book_access_code_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('book_access_code.settings');
    $form['access_page_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Access Page Description'),
      '#description' => $this->t('The description seen on the access code page.'),
      '#default_value' => $config->get('access_page_description'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('book_access_code.settings')
      ->set('access_page_description', $form_state->getValue('access_page_description'))
      ->save();
  }

}
