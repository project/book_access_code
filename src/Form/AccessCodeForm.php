<?php

namespace Drupal\book_access_code\Form;

use Drupal\book\BookManagerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for editing Access Codes.
 */
class AccessCodeForm extends ContentEntityForm {

  /**
   * BookManager service.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected $bookManager;

  /**
   * Constructs a BookOutlineForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\book\BookManagerInterface $book_manager
   *   The BookManager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, BookManagerInterface $book_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->bookManager = $book_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('book.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\book_access_code\Entity\AccessCodeInterface $entity */
    $entity = $this->entity;
    $form = parent::buildForm($form, $form_state);

    $options = [];

    foreach ($this->bookManager->getAllBooks() as $book) {
      $options[$book['nid']] = $book['title'];
    }

    $form['book'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Books'),
      '#description' => $this->t('This access code will provide access to the selected books.'),
      '#options' => $options,
      '#default_value' => $entity->getBookIds(),
      '#weight' => 13,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage(
        $this->t('Created the %label Book Access Code.', [
          '%label' => $entity->label(),
        ])
      );
    }
    else {
      $this->messenger()->addMessage(
        $this->t('Saved the %label  Book Access Code.', [
          '%label' => $entity->label(),
        ])
      );
    }

    $form_state->setRedirect('entity.access_code.collection');

    return $status;
  }

}
