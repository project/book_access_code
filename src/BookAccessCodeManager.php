<?php

namespace Drupal\book_access_code;

use Drupal\book\BookManagerInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\RedirectDestination;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a book access code manager.
 */
class BookAccessCodeManager implements BookAccessCodeManagerInterface {

  /**
   * The page cache kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  private $pageCacheKillSwitch;

  /**
   * The redirect destination.
   *
   * @var \Drupal\Core\Routing\RedirectDestination
   */
  private $redirectDestination;

  /**
   * The book access session manager.
   *
   * @var \Drupal\book_access_code\BookAccessSessionInterface
   */
  private $bookAccessSession;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The book manager.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  private $bookManager;

  /**
   * BookAccessCodeManager constructor.
   *
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $pageCacheKillSwitch
   *   The page cache kill switch.
   * @param \Drupal\Core\Routing\RedirectDestination $redirectDestination
   *   The redirect destination helper.
   * @param \Drupal\book_access_code\BookAccessSessionInterface $bookAccessSession
   *   The book access session handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\book\BookManagerInterface $bookManager
   *   The book manager.
   */
  public function __construct(KillSwitch $pageCacheKillSwitch, RedirectDestination $redirectDestination, BookAccessSessionInterface $bookAccessSession, EntityTypeManagerInterface $entityTypeManager, BookManagerInterface $bookManager) {
    $this->pageCacheKillSwitch = $pageCacheKillSwitch;
    $this->redirectDestination = $redirectDestination;
    $this->bookAccessSession = $bookAccessSession;
    $this->entityTypeManager = $entityTypeManager;
    $this->bookManager = $bookManager;
  }

  /**
   * {@inheritdoc}
   */
  public function nodeIsPartOfBook(NodeInterface $node): bool {
    return !empty($node->book['bid']);
  }

  /**
   * {@inheritdoc}
   */
  public function sendAccessDenied(int $bid): void {
    $this->pageCacheKillSwitch->trigger();
    $query = $this->redirectDestination->getAsArray();
    $query['bid'] = $bid;
    $response = new RedirectResponse(Url::fromRoute('book_access_code.book_access_code_form', [], ['query' => $query])->toString());
    $response->send();
  }

  /**
   * {@inheritdoc}
   */
  public function nodeIsPartOfAccessRestrictedBook(NodeInterface $node): bool {
    if (!$this->nodeIsPartOfBook($node)) {
      return FALSE;
    }

    return $this->bookIsAccessRestricted($node->book['bid']);
  }

  /**
   * {@inheritdoc}
   */
  public function bookIsAccessRestricted(int $bid): bool {
    return !empty($this->activeAccessCodesForBook($bid));
  }

  /**
   * {@inheritdoc}
   */
  public function userGrantedAccessToBook(int $bid): bool {
    return count(array_intersect($this->bookAccessSession->getAccessCodes(), $this->activeAccessCodesForBook($bid))) > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function accessCodeAppliesToBook(int $bid, string $accessCode): bool {
    return in_array($accessCode, $this->activeAccessCodesForBook($bid), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function storeAccessCodeToCurrentUserSession(string $accessCode): void {
    $this->bookAccessSession->addAccessCode($accessCode);
  }

  /**
   * Gets the active access codes for a book.
   *
   * @param int $bid
   *   The book id (node id).
   *
   * @return array
   *   Array of access codes.
   */
  private function activeAccessCodesForBook(int $bid): array {
    $codes = [];

    try {
      /** @var \Drupal\Core\Entity\EntityStorageInterface $accessCodeStorage */
      $accessCodeStorage = $this->entityTypeManager->getStorage('access_code');
    }
    catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      return [];
    }

    /** @var \Drupal\Core\Entity\Query\QueryInterface $entityQuery */
    $entityQuery = $accessCodeStorage->getQuery();
    $accessCodeIds = $entityQuery
      ->accessCheck(TRUE)
      ->condition('status', TRUE)
      ->condition('book.target_id', $bid)
      ->execute();

    if (empty($accessCodeIds)) {
      return [];
    }

    /** @var \Drupal\book_access_code\Entity\AccessCodeInterface[] $accessCodes */
    $accessCodes = $accessCodeStorage->loadMultiple($accessCodeIds);

    foreach ($accessCodes as $accessCode) {
      $codes[] = $accessCode->getCode();
    }

    return $codes;
  }

  /**
   * {@inheritdoc}
   */
  public function booksWithoutActiveAccessCodes(): array {
    $books = $this->bookManager->getAllBooks();

    foreach ($books as $bid => $book) {
      if ($this->bookIsAccessRestricted($bid)) {
        unset($books[$bid]);
      }
    }

    return $books;
  }

  /**
   * {@inheritdoc}
   */
  public function isBookId(int $bid): bool {
    return array_key_exists($bid, $this->bookManager->getAllBooks());
  }

}
