<?php

namespace Drupal\book_access_code\EventSubscriber;

use Drupal\book_access_code\BookAccessCodeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class KernelEventsSubscriber.
 */
class KernelEventsSubscriber implements EventSubscriberInterface {

  /**
   * The account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $account;

  /**
   * The route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * The book access code manager.
   *
   * @var \Drupal\book_access_code\BookAccessCodeManagerInterface
   */
  private $bookAccessCodeManager;

  /**
   * KernelEventsSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\book_access_code\BookAccessCodeManagerInterface $bookAccessCodeManager
   *   The book access code manager.
   */
  public function __construct(AccountInterface $account, RouteMatchInterface $routeMatch, BookAccessCodeManagerInterface $bookAccessCodeManager) {
    $this->account = $account;
    $this->routeMatch = $routeMatch;
    $this->bookAccessCodeManager = $bookAccessCodeManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    // This needs to run before RouterListener::onKernelRequest(), which has
    // a priority of 32. Otherwise, that aborts the request if no matching
    // route is found.
    $events[KernelEvents::RESPONSE][] = ['onRespond', 33];
    return $events;
  }

  /**
   * Determines access if viewing node that is part of a book.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond(ResponseEvent $event): void {
    if (!$event->isMainRequest()) {
      return;
    }

    if ($this->account->hasPermission('bypass book access code checks')) {
      return;
    }

    // Make sure we are looking at a node page (book pages are nodes).
    if ($this->routeMatch->getRouteName() !== 'entity.node.canonical') {
      return;
    }

    // Make sure we can actually load the node.
    if (!(($node = $this->routeMatch->getParameter('node')) && $node instanceof NodeInterface)) {
      return;
    }

    // If this node is not part of a book we do not have to do anything here.
    if (!$this->bookAccessCodeManager->nodeIsPartOfBook($node)) {
      return;
    }

    // If the book is not access restricted we don't need to do anything.
    if (!$this->bookAccessCodeManager->bookIsAccessRestricted($node->book['bid'])) {
      return;
    }

    if ($this->bookAccessCodeManager->userGrantedAccessToBook($node->book['bid'])) {
      // TODO: Check/change cachability here?
      return;
    }

    // If all of the above fails to return,
    // we do not have access to the book and should direct the visitor
    // to the access denied page.
    $this->bookAccessCodeManager->sendAccessDenied($node->book['bid']);

  }

}
