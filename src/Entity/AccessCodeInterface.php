<?php

namespace Drupal\book_access_code\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Access code entities.
 *
 * @ingroup book_access_code
 */
interface AccessCodeInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Access code name.
   *
   * @return string
   *   Name of the Access code.
   */
  public function getName(): string;

  /**
   * Sets the Access code name.
   *
   * @param string $name
   *   The Access code name.
   *
   * @return \Drupal\book_access_code\Entity\AccessCodeInterface
   *   The called Access code entity.
   */
  public function setName(string $name): AccessCodeInterface;

  /**
   * Gets the Access code creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Access code.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Access code creation timestamp.
   *
   * @param int $timestamp
   *   The Access code creation timestamp.
   *
   * @return \Drupal\book_access_code\Entity\AccessCodeInterface
   *   The called Access code entity.
   */
  public function setCreatedTime(int $timestamp): AccessCodeInterface;

  /**
   * Gets the nids (bookids) of the books this access code grants access to.
   *
   * @return array
   *   Array of book ids.
   */
  public function getBookIds(): array;

  /**
   * Gets the titles of the books this access code grants access to.
   *
   * @return array
   *   Array of book labels.
   */
  public function getBookLabels(): array;

  /**
   * Gets links to books this access code grants access to.
   *
   * @return \Drupal\Core\Link[]
   *   Array of book Links.
   */
  public function getBookLinks(): array;

  /**
   * Gets the code.
   *
   * @return string
   *   The code.
   */
  public function getCode(): string;

}
