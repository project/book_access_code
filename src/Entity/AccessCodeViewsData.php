<?php

namespace Drupal\book_access_code\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Access code entities.
 */
class AccessCodeViewsData extends EntityViewsData {

}
