<?php

namespace Drupal\book_access_code\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Access code entity.
 *
 * @ingroup book_access_code
 *
 * @ContentEntityType(
 *   id = "access_code",
 *   label = @Translation("Access code"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\book_access_code\AccessCodeListBuilder",
 *     "views_data" = "Drupal\book_access_code\Entity\AccessCodeViewsData",
 *     "access" = "Drupal\book_access_code\AccessCodeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\book_access_code\Form\AccessCodeForm",
 *       "add" = "Drupal\book_access_code\Form\AccessCodeForm",
 *       "edit" = "Drupal\book_access_code\Form\AccessCodeForm",
 *       "delete" = "Drupal\book_access_code\Form\AccessCodeDeleteForm",
 *     }
 *   },
 *   base_table = "access_code",
 *   translatable = FALSE,
 *   admin_permission = "administer access codes",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *     "code" = "code",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/book/access_code/add",
 *     "edit-form" = "/admin/structure/book/access_code/{access_code}/edit",
 *     "delete-form" = "/admin/structure/book/access_code/{access_code}/delete",
 *     "collection" = "/admin/structure/book/access_code",
 *   },
 * )
 */
class AccessCode extends ContentEntityBase implements AccessCodeInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): AccessCodeInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): AccessCodeInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Access code. Used for administrative purposes only.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -99,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Code'))
      ->setDescription(new TranslatableMarkup('The unique access code. Only use letters and numbers.'))
      ->addConstraint('UniqueField')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 5,
        'settings' => [
          'size' => 12,
          'placeholder' => 'ACCE55CODE',
        ],
      ])
      ->addPropertyConstraints('value', [
        'Regex' => [
          'pattern' => '/^[0-9a-zA-Z]+$/',
          'message' => 'Please use numbers and letters only',
        ],
        'Length' => [
          'min' => 4,
          'max' => 12,
          'minMessage' => 'The access code must be at least 4 letters and numbers',
          'maxMessage' => 'The access code can not be more than 12 letters and numbers',
        ],
      ])
      ->setRequired(TRUE);

    $fields['book'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Books'))
      ->setDescription(new TranslatableMarkup('The books that this code grants access to.'))
      ->setSetting('target_type', 'node')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE);

    $fields['status']
      ->setLabel(new TranslatableMarkup('Active'))
      ->setDescription(t('When checked this access code can be used.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 99,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookIds(): array {
    $bookIds = [];

    foreach ($this->get('book') as $book) {
      if ($book->target_id) {
        $bookIds[] = $book->target_id;
      }
    }

    return $bookIds;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookLabels(): array {
    $bookLabels = [];
    $bookNodes = $this->entityTypeManager()->getStorage('node')->loadMultiple($this->getBookIds());
    foreach ($bookNodes as $bookNode) {
      $bookLabels[$bookNode->id()] = $bookNode->label();
    }
    return $bookLabels;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookLinks(): array {
    $books = [];
    $bookNodes = $this->entityTypeManager()->getStorage('node')->loadMultiple($this->getBookIds());
    foreach ($bookNodes as $bookNode) {
      $books[$bookNode->id()] = $bookNode->toLink();
    }
    return $books;
  }

  /**
   * {@inheritdoc}
   */
  public function getCode(): string {
    return $this->get('code')->value;
  }

}
