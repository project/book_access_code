<?php

namespace Drupal\book_access_code;

use Drupal\node\NodeInterface;

/**
 * Provides an interface defining a book access code manager.
 */
interface BookAccessCodeManagerInterface {

  /**
   * Check if a node is part of a book.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to check if it is part of a book.
   *
   * @return bool
   *   TRUE if node is part of a book, FALSE otherwise.
   */
  public function nodeIsPartOfBook(NodeInterface $node): bool;

  /**
   * Check if a node is part of an access restricted book.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to check if it is part of an access restricted book.
   *
   * @return bool
   *   TRUE if node is part of an access restricted book, FALSE otherwise.
   */
  public function nodeIsPartOfAccessRestrictedBook(NodeInterface $node): bool;

  /**
   * Send access denied.
   *
   * @param int $bid
   *   The book id (nid) to check.
   */
  public function sendAccessDenied(int $bid): void;

  /**
   * Determines if book has access restricted codes attached.
   *
   * @param int $bid
   *   The book id (nid) to check.
   *
   * @return bool
   *   TRUE if access restricted, FALSE otherwise.
   */
  public function bookIsAccessRestricted(int $bid): bool;

  /**
   * Determine if the user has already been granted access to the book.
   *
   * @param int $bid
   *   The book id (nid) to check.
   *
   * @return bool
   *   TRUE if already granted access, FALSE otherwise.
   */
  public function userGrantedAccessToBook(int $bid): bool;

  /**
   * Checks to see if access code applies to the book.
   *
   * @param int $bid
   *   The book id (nid) to check.
   * @param string $accessCode
   *   Provided Access Code.
   *
   * @return bool
   *   TRUE if provides access to book, FALSE otherwise.
   */
  public function accessCodeAppliesToBook(int $bid, string $accessCode): bool;

  /**
   * Stores an access code to the users session.
   *
   * @param string $accessCode
   *   The access code.
   */
  public function storeAccessCodeToCurrentUserSession(string $accessCode): void;

  /**
   * Gets array of all books that do not have an active access code.
   *
   * The array has the same struture as \Drupal\book\BookManager::getAllBooks().
   *
   * @see \Drupal\book\BookManager::getAllBooks()
   *
   * @return array
   *   An array of books.
   */
  public function booksWithoutActiveAccessCodes(): array;

  /**
   * Check to see if an id is a book id.
   *
   * @param int $bid
   *   The book id to check.
   *
   * @return bool
   *   TRUE if is a book, FALSE otherwise.
   */
  public function isBookId(int $bid): bool;

}
