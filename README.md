# Book Access Code

The Book Access Code module adds the ability to use access codes to grant read access to Books. This adds functionality
to Drupal core's book module.

## Installation

   ```sh
   composer require 'drupal/book_access_code:^1.0'
   ```

## Configuration

- Edit the access denied, access code required message at `/admin/config/system/book_access_code/settings`.
- Add and manage book access codes at `/admin/structure/book/access_code`
- Permissions can be added to any role to allow them to bypass the book access code. This permission can be helpful for
content editor roles.
